package com.hcl.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Account {
	
	 	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Long id;
	    private int accountNumber;
	    private long accountBalance;
	    @OneToMany(mappedBy="account")
	    private List<Banificiary> banificiary;
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public int getAccountNumber() {
			return accountNumber;
		}
		public void setAccountNumber(int accountNumber) {
			this.accountNumber = accountNumber;
		}
		public long getAccountBalance() {
			return accountBalance;
		}
		public void setAccountBalance(long accountBalance) {
			this.accountBalance = accountBalance;
		}
		public List<Banificiary> getBanificiary() {
			return banificiary;
		}
		public void setBanificiary(List<Banificiary> banificiary) {
			this.banificiary = banificiary;
		}

}
